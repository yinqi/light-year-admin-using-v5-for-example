# Light Year Admin Using v5 For Example

#### 介绍
该项目是光年模板的一些示例以及其他插件的整合。 有些插件整合用到了ajax调用后端程序处理(虽然有些只是直接返回成功)，这里用的是php，需要在应用服务器中浏览测试效果。

[猛戳这里去v5的iframe版本](https://gitee.com/yinqi/Light-Year-Admin-Using-Iframe-v5)

[猛戳这里去v5的非iframe版本](https://gitee.com/yinqi/Light-Year-Admin-Template-v5)

#### 示例演示
http://lyear.itshubao.com/v5/example/

#### 整合插件列表
- ckeditor 富文本编辑器
- dropzone 可拖拉文件异步上传控件
- editor.md 在线 Markdown 编辑器
- jquery-auto-complete 自动补全插件
- jquery-toolbar jQuery工具栏插件
- jquery-validate JQuery表单验证插件
- select2 下拉框插件
- tinymce 轻量级的所见即所得编辑器
- ueditor 开源的在线HTML编辑器
- wangEditor 轻量级web富文本编辑器
- webuploader 百度fex 团队的上传插件 web uploader
- ztree_v3 树状结构插件，这里单独根据项目的字体改写了一个样式皮肤
- jquery.lyear.loading 自己凑合写的一个加载等待动画插件
- fontIconPicker 一款图标选择插件，可以根据自己所用的字体来配置操作
- cropper 图片裁剪插件，示例中做了裁剪以及bse64上传的效果
- material_datetimepicker Bootstrap谷歌Material Design风格日期时间选择器
- malength 字符输入控制插件
- touchspin 输入递增/递减组件。它支持鼠标滚轮和上/下键
- daterangepicker 时间段选择器，非常好用
- layer web弹层组件
- bootstrap-treeview 树状插件
- jquery.raty 评分插件
- sliderVerification 滑块验证
- imgVer 图片滑块验证
- bootstrap3-dialog 模态框增强插件
- jstree 树状结构插件
- summernote 富文本编辑器
- fixedheadertable 固定表头或列插件